;; Indicate where your `dot-out-files.el' is located:
(load-file "/path/to/your/dot-out-files.el")

;; Process .out files in a given directory:
(process-out-files "/path/to/your/out/files/" ; directory where the .out files are located (it must end by a "/", like here)
                   nil)                       ; replace nil by t if you do not want the output csv file to be automatically written
