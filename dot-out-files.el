;;; dot-out-files.el --- Elisp functions for extracting information in .out
;;; (qpWave log) files.

;; Author: Frédéric Santos, 2020
;; Created: 13 may 2020
;; Homepage: https://gitlab.com/f-santos/elisp-for-out-files
     
;;; Commentary:
;;; See the GitLab repo for more explanations.
     
;;; Code:
;; Primitive functions:
(defun f4--extract-individuals (buffer)
  "Find the two individuals (left pops) in a given BUFFER."
  (interactive "bFind individuals in buffer: ")
  (let ((beg nil)
        (indiv-names nil))
    (with-current-buffer buffer
      (setq buffer-read-only t)
      (goto-char (point-min))
      (re-search-forward "left pops:")
      (forward-line)
      (setq beg (point))
      (forward-line)
      (move-end-of-line nil)
      (setq indiv-names
            (replace-regexp-in-string
             "\n"
             " "
             (buffer-substring-no-properties beg (point)))))
    indiv-names))

(defun f4--extract-pvalue (buffer)
  "Extract the F4 p-value in a given BUFFER."
  (interactive "bFind F4 p-value in buffer: ")
  (let ((beg nil)
        (pval nil))
    (with-current-buffer buffer
      (setq buffer-read-only t)
      (goto-char (point-min))
      (re-search-forward "tail:[[:space:]]*" nil nil 1)
      (setq beg (point))
      (re-search-forward "[0-9]\.?[e0-9\-]*\\>" nil t 1)
      (setq pval (buffer-substring-no-properties beg (point))))
    pval))

(defun f4--process-file (file)
  "Extract indiv names and pval in a given FILE."
  (interactive "FFind F4 p-value in file: ")
  (let ((result nil)
        (buffer (find-file-noselect file)))
    (setq result (concat (f4--extract-individuals buffer)
                         " "
                         (f4--extract-pvalue buffer)))
    result))

(defun process-out-files (directory &optional dontwrite)
  "Process all .out files in a DIRECTORY.
I.e., return in a buffer indiv names and p-values extracted from
all .out files in this directory.
This buffer is also saved in a file result.csv, unless the user
set DONTWRITE to t."
  (interactive "DChoose a directory: ")
  (let* ((list-out-files (directory-files directory t ".out\\'"))
         (result-file (concat directory "result.csv"))
         (buffer (create-file-buffer result-file)))
    (with-current-buffer buffer
      (while list-out-files
        (insert (f4--process-file (car list-out-files)))
        (insert "\n")
        (forward-line)
        (setq list-out-files (cdr list-out-files))))
    (switch-to-buffer-other-window buffer)
    (unless dontwrite
      (write-file result-file))))

(provide 'dot-out-files)
;;; dot-out-files.el ends here
